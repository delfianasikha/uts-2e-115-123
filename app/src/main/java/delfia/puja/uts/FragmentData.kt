package delfia.puja.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_data.*
import kotlinx.android.synthetic.main.activity_data.view.*
import kotlinx.android.synthetic.main.activity_paket.view.*

class FragmentData: Fragment(),View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner2.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c: Cursor = spAdapter.getItem(position) as Cursor
        namaPaket = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnDeleteData -> {
                dialog.setTitle("Konfirmasi").setMessage("Anda yakin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btndel)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateData -> {
                dialog.setTitle("Konfirmasi").setMessage("Data yang dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnup)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsertData -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data sudah benar?")
                    .setPositiveButton("Ya",btnIns)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter: ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v: View
    var namaData: String = ""
    var namaPaket: String = ""
    var alamatData: String = ""
    lateinit var db: SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.activity_data, container, false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteData.setOnClickListener(this)
        v.btnInsertData.setOnClickListener(this)
        v.btnUpdateData.setOnClickListener(this)
        v.spinner2.onItemSelectedListener = this
        v.lsData.setOnItemClickListener(itemClick)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataData("")
        showDataPaket()
    }

    fun showDataData(namaData: String) {
        var sql = ""
        if (!namaData.trim().equals("")) {
            sql =
                "select m.nama as _id, m.alamat, p.nama_paket from dt m, pkt p where m.id_Paket=p.id_Paket and m.nama like '%$namaData%'"

        } else {
            sql =
                "select m.nama as _id, m.alamat, p.nama_paket from dt m, pkt p where m.id_Paket=p.id_Paket order by m.nama asc"
        }
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent, R.layout.item_data, c,
            arrayOf("_id", "alamat","nama_paket"),
            intArrayOf(R.id.txNamaData, R.id.txAlamat, R.id.txPaketData),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsData.adapter = lsAdapter
    }

    fun showDataPaket() {
        val c: Cursor =
            db.rawQuery("select nama_paket as _id from pkt order by nama_paket asc", null)
        spAdapter = SimpleCursorAdapter(
            thisParent,
            android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = spAdapter
        v.spinner2.setSelection(0)
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        namaData = c.getString(c.getColumnIndex("_id"))
        v.txNamaData.setText(c.getString(c.getColumnIndex("_id")))
        v.edAlamat.setText(c.getString(c.getColumnIndex("alamat")))
    }

    fun insertDataData(namaData: String, alamat: String, id_Paket: Int)
    {
        var sql = "insert into dt(nama,alamat,id_Paket) values (?,?,?)"
        db.execSQL(sql, arrayOf(namaData, alamat,id_Paket))
        showDataData("")
    }

    fun updateDataData(namaData: String, alamatData: String,id_Paket: Int){
        var cv : ContentValues = ContentValues()
        cv.put("alamat",alamatData)
        cv.put("id_Paket",id_Paket)
        db.update("dt",cv,"nama='$namaData'",null)
        showDataData("")
    }

    fun deleteDataData(namaData: String){
        db.delete("dt","nama = '$namaData'",null)
        showDataData("")
    }

    val btnup = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_Paket from pkt where nama_paket = '$namaPaket'"
        val c : Cursor = db.rawQuery(sql, null)
        if (c.count > 0){
            c.moveToFirst()
            updateDataData(
                v.txNamaData.text.toString(),v.edAlamat.text.toString(),
                c.getInt(c.getColumnIndex("id_Paket"))
            )
            v.txNamaData.setText("")
            v.edAlamat.setText("")
        }
    }

    val btndel = DialogInterface.OnClickListener { dialog, which ->
        deleteDataData(namaData)
        v.edAlamat.setText("")
        v.txNamaData.setText("")
    }

    val btnIns = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_Paket from pkt where nama_paket='$namaPaket'"
        val c: Cursor = db.rawQuery(sql, null)
        if (c.count > 0){
            c.moveToFirst()
            insertDataData(
                v.txNamaData.text.toString(), v.edAlamat.text.toString(),
                c.getInt(c.getColumnIndex("id_Paket"))
            )
            v.txNamaData.setText("")
            v.edAlamat.setText("")
        }
    }
}

