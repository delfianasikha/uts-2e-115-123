package delfia.puja.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import kotlinx.android.synthetic.main.activity_paket.view.*
import kotlinx.android.synthetic.main.item_data_paket.*

class FragmentPaket : Fragment(),View.OnClickListener{
    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btnInsert ->{
                builder.setTitle("konfirmasi").setMessage("Data yang anda masukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("konfirmasi").setMessage("Anda yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("konfirmasi").setMessage("Data yang anda masukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpadteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter: ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idPaket : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.activity_paket,container,false)
        v.btnDelete.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnUpdate.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsPaket.setOnItemClickListener(itemClick)

        return v
    }
    fun showDataPaket(){
        val cursor : Cursor = db.query("pkt", arrayOf("nama_paket","id_paket as _id"),
            null,null,null,null,"nama_paket asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_paket,cursor,
            arrayOf("_id","nama_paket"), intArrayOf(R.id.txIdPaket, R.id.txNamaPaket),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
            v.lsPaket.adapter = adapter

    }

    override fun onStart( ) {
        super.onStart()
       showDataPaket()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idPaket = c.getString(c.getColumnIndex("_id"))
        v.edPaket.setText(c.getString(c.getColumnIndex("nama_paket")))
    }
    fun insertDataPaket(namaPaket : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_paket",namaPaket)
        db.insert("pkt",null,cv)
        showDataPaket()
    }
     fun updateDataPaket(namaPaket: String, idPaket: String){
         var cv : ContentValues = ContentValues()
         cv.put("nama_paket",namaPaket)
         db.update("pkt",cv,"id_paket = $idPaket",null)
         showDataPaket()
     }
     fun deleteDataPaket(idPaket: String){
         db.delete("pkt","id_paket = $idPaket",null)
         showDataPaket()
     }
    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataPaket(v.edPaket.text.toString())
        v.edPaket.setText("")
    }
    val btnUpadteDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataPaket(v.edPaket.text.toString(),idPaket)
        v.edPaket.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataPaket(idPaket)
        v.edPaket.setText("")
    }
}