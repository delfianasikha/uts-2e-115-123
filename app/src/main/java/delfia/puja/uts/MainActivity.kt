package delfia.puja.uts

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragPaket : FragmentPaket
    lateinit var fragData : FragmentData
    lateinit var  ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragData = FragmentData()
        fragPaket = FragmentPaket()
        db = DBOpenHelper(this).writableDatabase
    }
    fun getDBObject() : SQLiteDatabase{
        return db
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemPaket ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.layout,fragPaket).commit()
                layout.setBackgroundColor(Color.argb(245,255,255,225))
                layout.visibility = View.VISIBLE
            }
            R.id.itemData ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.layout,fragData).commit()
                layout.setBackgroundColor(Color.argb(245,255,255,225))
                layout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> layout.visibility = View.GONE
        }
        return true
    }
}
