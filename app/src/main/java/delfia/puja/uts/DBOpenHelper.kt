package delfia.puja.uts

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context, DB_Name,null, DB_Ver) {
    override fun onCreate(db: SQLiteDatabase?) {
        val tData = "create table dt(nama text primary key, alamat text not null, id_Paket int not null)"
        val tPaket = "create table pkt(id_Paket integer primary key autoincrement, nama_paket text not null)"
        val insPaket = "insert into pkt(nama_paket) values('bulan'),('minggu')"
        db?.execSQL(tData)
        db?.execSQL(tPaket)
        db?.execSQL(insPaket)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object{
        val DB_Name = "data"
        val DB_Ver = 1
    }
}